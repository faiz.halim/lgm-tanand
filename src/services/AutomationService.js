import BaseService from "@/services/BaseService";

export default class AutomationService extends BaseService {
	/**
	 * Get current batch by stage.
	 *
	 * @param stage
	 * @returns {Promise<*>}
	 */
	getCurrentBatchByStage(stage) {
		let url = this.baseURL + `api/automation/batch/current/${stage}`;

		return this.axios
			.get(url)
			.then(response => {
				return response;
			})
			.catch(error => {
				console.error(error);
				return error;
			});
	}

	getProcessSetting(processId = null, productId = null) {
		let url = this.baseURL + `api/automation/process/settings`;
		let params = {
			processId: processId,
			productId: productId
		};

		return this.axios
			.get(url, { params })
			.then(response => {
				return response;
			})
			.catch(error => {
				console.error(error);
				return error;
			});
	}

	updateProcessSetting(productId, processId, parameterId, value) {
		let url =
			this.baseURL +
			`api/setting/${productId}/process/${processId}/param/${parameterId}/${value}`;

		return this.axios
			.put(url)
			.then(response => {
				return response;
			})
			.catch(error => {
				console.error(error);
				return error;
			});
	}
}
