
import BaseService from "@/services/BaseService";

export default class UserTrailService extends BaseService {

    /**
     * Get alert data from the API.
     */
    getUserTrails(offset, limit) {
        let url = this.baseURL + `api/trail/user/${offset}/${limit}`;
        // {?type}
        // Available values : login, add_user, turn_on_off, set_sv, set_inverter_hz, set_modulating_valve, stop_batching, start_batch, stop_batch, next_batch, next_process, set_process_sv, update_threshold, delete_threshold, set_benchmark, update_oee_device, top_up_material, fill_up_material, change_device_mode, change_auto_mode, update_process_setting, archive_employee

        return this.axios.get(url)
            .then(response => {
                return response.data;
            }).catch(error => {
                console.error(error);
                return error;
            })
    }
}
