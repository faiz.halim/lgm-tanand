import BaseService from "@/services/BaseService";

export default class RecipeService extends BaseService {
    /**
     * Get recipe by product.
     * Available product ID
     * - DPNR
     * - ENR-25
     * - ENR-50
     *
     * @param productID Boolean
     *
     * @returns {Promise<*>}
     */
    getRecipeByProduct(productID) {
        let url = this.baseURL + `api/recipe/list/${productID}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     *
     * @param deviceID
     * @param startTimestamp
     * @param endTimestamp
     * @param interval
     * @returns {Promise<*>}
     */
    getRecipeBreakdown(deviceID, startTimestamp, endTimestamp, interval) {
        let url = this.baseURL + `api/recipe/breakdown/${deviceID}/${startTimestamp}/${endTimestamp}/${interval}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    getRecipeBreakdownOfCurrentBatch(productId) {
        let url = this.baseURL + `api/recipe/breakdown/batch/current`;
        let params = {
            productId: productId
        }

        return this.axios.get(url, { params }).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    getRecipeBreakdownOfBatches(offset, limit, productId) {
        let url = this.baseURL + `api/recipe/breakdown/batches/${offset}/${limit}`;
        let params = {
            productId: productId
        }

        return this.axios.get(url, { params }).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     *
     * @param obj
     * @returns {Promise<*>}
     */
    updateProductRecipe(obj) {
        let url = this.baseURL + `api/setting/recipe`;

        return this.axios.put(url, obj).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }
}