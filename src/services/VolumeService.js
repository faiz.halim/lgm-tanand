import BaseService from "@/services/BaseService";

export default class VolumeService extends BaseService {
    /**
     * Get volume by device id.
     *
     * @param deviceId
     * @param start long
     * @param end long
     * @param interval int
     *
     * @returns {Promise<*>}
     */
    getVolumeByDevice(deviceId, start, end, interval) {
        let url = this.baseURL + `api/volume/${deviceId}/${start}/${end}/${interval}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }
}