import BaseService from "@/services/BaseService";

export default class TemperatureService extends BaseService {
    /**
     * Get valve % by device id.
     *
     * @param deviceId
     * @param start long
     * @param end long
     * @param interval int
     *
     * @returns {Promise<*>}
     */
    getTemperatureReadingByDevice(deviceId, start, end, interval) {
        let url = this.baseURL + `api/temperature/reading/${deviceId}/${start}/${end}/${interval}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    getHumidityReadingByDevice(deviceId, start, end, interval) {
        let url = this.baseURL + `api/temperature/humidity/reading/${deviceId}/${start}/${end}/${interval}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }
}