import BaseService from "@/services/BaseService";

export default class ProductService extends BaseService {
    /**
     * Get product list.
     *
     * @returns {Promise<*>}
     */
    getProductList() {
        let url = this.baseURL + 'api/product/list';

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * Get camera material
     *
     * @returns {Promise<*>}
     */
    getCamMaterial(deviceId, start, end, group) {
        let url = this.baseURL + `api/product/cam/material/${deviceId}/${start}/${end}/${group}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    getTrolleyTotalCount(start, end, group) {
        let url = this.baseURL + `api/product/trolley/speed/average/${start}/${end}/${group}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    getDurationPerTrolley(start, end) {
        let url = this.baseURL + `api/product/trolley/duration/${start}/${end}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    getOutput(start, end, interval) {
        let url = this.baseURL + `api/product/output/${start}/${end}/${interval}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }
}