import BaseService from "@/services/BaseService";

export default class ValveService extends BaseService {
    /**
     * Get valve % by device id.
     *
     * @param deviceId
     * @param start long
     * @param end long
     * @param interval int
     *
     * @returns {Promise<*>}
     */
    getDeviceOpeningValve(deviceId, start, end, interval) {
        let url = this.baseURL + `api/valve/opening/${deviceId}/${start}/${end}/${interval}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }
}