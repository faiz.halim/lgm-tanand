import BaseService from "@/services/BaseService";

export default class UserService extends BaseService {
    /**
     * Set reset password email.
     *
     * @param email
     * @returns {Promise<* | void>}
     */
    sendResetPasswordEmail(email) {
        let url = this.baseURL + `api/user/auth/forgot/password/request/${email}`;

        return this.axios.get(url)
            .then(response => {
                return response;
            }).catch(error => {
                console.error(error)
                return error
            })
    }

    /**
     * Reset user password.
     *
     * @param email
     * @param newPassword
     * @param verificationCode
     *
     * @returns {Promise<* | void>}
     */
    resetPassword(email, newPassword, verificationCode) {
        let url = this.baseURL + "api/user/auth/forgot/password/reset";

        return this.axios.post(url, {
            "email": email,
            "password": newPassword,
            "verificationCode": verificationCode
        }) .then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        })
    }
}