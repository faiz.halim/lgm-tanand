import axios from 'axios'

/**
 * Base Service
 */
export default class BaseService {
    constructor(publicURL = false) {
        this.baseURL = process.env.VUE_APP_API_URL

        // Create axios instance.
        this.axios = axios.create();

        // If public URL no need to set Authorization Header.
        if (publicURL) {
            return;
        }

        // Axios interceptor to set Authorization Token in the header.
        this.axios.interceptors.request.use(
            async config => {
                const token = window.localStorage.getItem('token')

                config.headers = {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                }

                return config;
            },
            error => {
                Promise.reject(error)
            });

        // Check if response return 403 status. If true refresh the access token.
        this.axios.interceptors.response.use((response) => {
            return response
        }, async (error) => {
            const originalRequest = error.config;

            if (error.response.status === 403 && !originalRequest._retry) {
                originalRequest._retry = true;

                // Refresh access token to get a new token.
                const newToken = await this.refreshAccessToken();

                // Set Authorization Token Header.
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + newToken;

                return this.axios(originalRequest);
            }
            return Promise.reject(error);
        });
    }

    /**
     * Refresh access token.
     * Used in axios interceptor.
     */
    refreshAccessToken() {
        let refreshTokenURL = process.env.VUE_APP_API_URL + 'api/auth/refresh';

        this.axios.post(refreshTokenURL, {
            'refreshToken': window.localStorage.getItem('refresh_token')
        }).then(function (response) {
            if (response.status === 200) {
                window.localStorage.setItem('token', response.data.data.tokens.idToken);
            }
        }).catch(function (error) {
            console.error("Failed to refresh token.", error);
        });
    }
}