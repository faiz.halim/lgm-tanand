import BaseService from "@/services/BaseService";

export default class AuthService extends BaseService {
    /**
     * Login user into application.
     *
     * @param email
     * @param password
     *
     * @returns {Promise<* | void>}
     */
    login(email, password) {
        let url = this.baseURL + 'api/auth/login';

        return this.axios.post(url, {
            'email': email,
            'password': password
        }).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }
}