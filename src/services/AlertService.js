import BaseService from "@/services/BaseService";

/**
 * Alert Service.
 */
export default class AlertService extends BaseService {
    /**
     * Get Total Number Of Alert Logs Available.
     */
    countTotalAlert() {
        let url = this.baseURL + 'api/alert/count/active';

        return this.axios.get(url)
            .then(response => {
                return response.data;
            }).catch(err => {
                console.error(err)
                return err;
            })
    }

    /**
     * Get alert data from the API.
     */
    getAlertData(pageNumber, pageLimit, productID = 'DPNR') {
        let url = this.baseURL + `api/alert/list/${pageNumber}/${pageLimit}?createdTimestamp=DESC&productId=${productID}`;

        return this.axios.get(url)
            .then(response => {
                return response.data;
            }).catch(error => {
                console.error(error);
                return error;
            })
    }
}
