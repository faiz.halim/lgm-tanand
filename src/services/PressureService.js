import BaseService from "@/services/BaseService";

export default class PressureService extends BaseService {
    /**
     * Get pressure reading chart by device
     *
     * @returns {Promise<*>}
     */
    getPressureReadingByDevice(deviceId, start, end, interval) {
        let url = this.baseURL + `api/pressure/reading/${deviceId}/${start}/${end}/${interval}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }
}