import BaseService from "@/services/BaseService";

export default class ControlService extends BaseService {
    /**
     * Start Batch
     *
     * @param stage String
     * @param productID Boolean
     *
     * @returns {Promise<*>}
     */
    startBatch(stage, productID) {
        let url = this.baseURL + `api/cmnd/batch/${stage}/start/${productID}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * Stop Batch
     *
     * @param stage String
     *
     * @returns {Promise<*>}
     */
    stopBatch(stage) {
        let url = this.baseURL + `api/cmnd/batch/${stage}/stop`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * Start next batch process
     *
     * @param stage String
     *
     * @returns {Promise<*>}
     */
    startNextBatchProcess(stage) {
        let url = this.baseURL + `api/cmnd/batch/${stage}/next`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * Change automation mode to auto or manual.
     *
     * @param accessCode String
     * @param auto Boolean
     * @param userID Numeric
     * @returns {Promise<*>}
     */
    changeAutomationMode(accessCode, auto, userID) {
        let url = this.baseURL + 'api/cmnd/automation/change';

        return this.axios.post(url, {
            'accessCode': accessCode,
            'auto': auto,
            'userId': userID
        }).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * Set device inverter Hz. Not all device able to update the inverter Hz.
     *
     * @param deviceID
     * @param value
     * @returns {Promise<*>}
     */
    setInverterHz(deviceID, value) {
        let url = this.baseURL + `api/cmnd/device/${deviceID}/setInverterHz/${value}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * TopUp material. For example water.
     *
     * @param deviceID
     * @param value
     * @returns {Promise<*>}
     */
    topUpMaterial(deviceID, value) {
        let url = this.baseURL + `api/cmnd/device/${deviceID}/topup/${value}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * Set device state.
     * - on
     * - off
     *
     * @param deviceID
     * @param value
     * @returns {Promise<*>}
     */
    setDeviceState(deviceID, value) {
        let state = 'on';

        if (value === false) {
            state = 'off';
        }

        let url = this.baseURL + `api/cmnd/device/${deviceID}/state/${state}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * Set device value.
     * - on
     * - off
     *
     * @param deviceID
     * @param value
     * @returns {Promise<*>}
     */
    setDeviceValue(deviceID, value) {
        let url = this.baseURL + `api/cmnd/device/${deviceID}/setValue/${value}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

     /**
     * Set device value.
     * - on
     * - off
     *
     * @param deviceId
     * @param value
     * @returns {Promise<*>}
     */
    setModulatingValue(deviceId, value) {
        let url = this.baseURL + `api/cmnd/device/${deviceId}/setModulatingValue/${value}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * Change device mode.
     * - heating
     * - cooling
     *
     * @param deviceID
     * @param mode
     * @returns {Promise<*>}
     */
    changeDeviceMode(deviceID, mode) {
        let url = this.baseURL + `api/cmnd/device/${deviceID}/changeMode/${mode}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }

    /**
     * Change sv for enr auto mode.
     * - EnrHeating
     * - EnrCooling
     *
     * @param processId
     * @param value
     * @returns {Promise<*>}
     */
    setProcessSv(processId, value) {
        let url = this.baseURL + `api/cmnd/batch/${processId}/setSv/${value}`;

        return this.axios.get(url).then(response => {
            return response;
        }).catch(error => {
            console.error(error)
            return error
        });
    }    
}