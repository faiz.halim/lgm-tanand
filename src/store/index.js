import { getField, updateField } from 'vuex-map-fields';
import createPersistedState from "vuex-persistedstate";

export default {
    state: {
        pendingPrepEnrProcess: null,
        pendingPrepDpnrProcess: null,
        pendingProdProcess: null,
        selectedPendingProcess: null,
        prepDPNR: 'n/a',
        prepENR: 'n/a',
        prod: 'n/a',
        mode: 'auto',
        prepDpnrBatch: null
    },
    getters: {
        // Add the `getField` getter to the
        // `getters` of your Vuex store instance.
        getField,
    },
    mutations: {
        updateField
    },
    plugins: [createPersistedState()]
}