export const PROCESS = {
    "DPNR": "DPNR",
    "ENR25": "ENR-25",
    "ENR50": "ENR-50",
};

export const CHART_INTERVAL = 600000
