export default {
    methods : {
        /**
         * Show loading overlay.
         */
        showLoadingOverlay() {
            this.$JsLoadingOverlay.show({'spinnerIcon': 'ball-scale-multiple'});
        },

        /**
         * Hide loading overlay.
         */
        hideLoadingOverlay() {
            this.$JsLoadingOverlay.hide();
        },

        /**
         * Show modal.
         *
         * @param modalReference
         */
        showModal(modalReference) {
            this.$refs[modalReference].open();
        },

        /**
         * Hide modal.
         *
         * @param modalReference
         */
        hideModal(modalReference) {
            this.$refs[modalReference].close();
        },

        /**
         * Display success popup.
         *
         * @param message
         */
        displaySuccessPopup(message) {
            this.$swal({
                title: "Success",
                html: message,
                icon: "success",
                confirmButtonText: "OK"
            });
        },

        /**
         * Display error message in popup.
         * @param result
         */
        displayErrorPopup(result) {
            this.$swal({
                title: "Error!",
                text: result.response.data.message,
                icon: "error",
                confirmButtonText: "OK"
            });
            this.hideLoadingOverlay();
        },

        /**
         * Check object empty or not.
         *
         * @param obj
         * @returns {boolean}
         */
        checkObjectEmpty(obj) {
            for (let i in obj) {
                return false;
            }

            return true
        },

        /**
         * Uppercase first letter
         *
         * @returns {string}
         */
        ucFirst(word) {
            return word.substr(0, 1).toUpperCase() + word.substr(1).toLowerCase();
        },

        /**
         * Convert camelCaseText to Sentence Case Text
         *
         * @returns {string}
         */
        camelToSpace(text) {
            var result = text.replace( /([A-Z])/g, " $1" );
            return result.charAt(0).toUpperCase() + result.slice(1);
        }
    }
}