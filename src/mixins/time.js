export default {
    methods: {
        timestampToTime(timestamp) {
            if (timestamp <= 0) {
                return "-";
            }

            const dateObject = new Date(timestamp);
            var month = dateObject.getMonth() + 1;
            var date = dateObject.getDate();
            var hours = dateObject.getHours();
            var minutes = "" + dateObject.getMinutes();

            if (date < 10) {
                date = "0" + date;
            }
            if (month < 10) {
                month = "0" + month;
            }
            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }

            return `${hours}:${minutes}`;
        },
        
        /**
         * Display elapsed time
         * @param startTimestamp
         */
        calculateElapsedTime(startTimestamp, endTimestamp) {
            if (endTimestamp === 0 || endTimestamp === undefined || endTimestamp === null) {
                endTimestamp = new Date().getTime();
            }
            return this.displaySecondInHHmm((endTimestamp - startTimestamp) / 1000)
        },

        /**
         * Display seconds in day hh:mm
         * @param values
         */
        displaySecondInHHmm(values) {
            if (isNaN(values) || values === 0) return "-";

            let day = Math.floor(values / (3600 * 24));
            let hour = Math.floor(values % (3600 * 24) / 3600);
            let minutes = this.checkTime(Math.floor(values % 3600 / 60));
            // let seconds = checkTime(Math.floor(values % 3600 % 60));

            return `${day !== 0 ? day + ' days ' : ""}${hour}hr ${minutes}min`;
        },

        /**
         * Add leading zero if < 10
         * @param i
         */
        checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        },

        get12AMTimestamp(timestamp) {
            const dateObject = new Date(timestamp);
            dateObject.setHours(0, 0, 0, 0);
        
            return dateObject.getTime();
        },
        
        get1159PMTimestamp(timestamp) {
            const dateObject = new Date(timestamp);
            dateObject.setHours(23, 59, 59, 0);
        
            return dateObject.getTime();
        },
        
    }
}