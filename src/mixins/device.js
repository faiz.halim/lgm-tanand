// Vue mixins
export default {
	methods: {
		/**
		 * To set tank level fill up level based on load cell weight
		 * Tank is fix to 12000KG
		 */
		setTankLevel(deviceName) {
			if (this.device[deviceName] === undefined) {
				return 0
			} else {
				return ((this.device[deviceName].gross / 12000) * 100).toFixed(0);
			}
		},

		/**
		 * To set device icon color. Mostly used for svg icon.
		 *  For example want to set pump color.
		 *  The data coming from lgm/device/BTP1.
		 *
		 *  Got 3 different colors
		 *  - On: Green
		 *  - Off: Grey
		 *  - Trip: Red
		 */
		setDeviceIconStatus(deviceName) {
			if (this.device[deviceName] === undefined) {
				return 'off';
			}

			if (this.device[deviceName].isTripped) {
				return 'trip';
			}

			if (this.device[deviceName].isOn) {
				return 'on';
			}

			if (!this.device[deviceName].isOn) {
				return 'off';
			}

			return 'off'
		},


		/**
		 * To set device icon color. Mostly used for svg icon.
		 *  For example want to set pump color.
		 *  The data coming from lgm/data/XSV114/status.
		 *
		 *  Got 2 different colors
		 *  - On: Green
		 *  - Off: Grey
		 */
		setDeviceIconStatusOtherType(deviceName) {
			if (this.device[deviceName] === undefined) {
				return 'off';
			}

			if (this.device[deviceName].status === 1) {
				return 'on';
			}

			if (!this.device[deviceName].status === 0) {
				return 'off';
			}

			return 'off'
		},

		/**
		 * To set device icon color. Mostly used for background color.
		 *  For example want to set pump color.
		 *  The data coming from lgm/data/XSV114/status.
		 *
		 *  Got 2 different colors
		 *  - On: Green
		 *  - Off: Grey
		 */
		setDevicePanelStatus(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '#F2F2F2'
			}

			if (this.device[deviceName].status === 1) {
				return '#7ed321';
			}

			if (!this.device[deviceName].status === 0) {
				return '#F2F2F2'
			}

			return '#F2F2F2'
		},

		/**
		 * To set device icon color. Mostly used for background color.
		 *  For example want to set pump color.
		 *  The data coming from lgm/data/XSV114/status.
		 *
		 *  Got 2 different colors
		 *  - status 1: Red
		 *  - otherwise: Grey
		 */
		setDevicePanelAlarmStatus(deviceName) {
			if (this.device[deviceName] === undefined) {
				return
			}

			if (this.device[deviceName].status === 1) {
				return "panel-trip"
			}

			return
		},

		/**
		 * Set device status text whether off, on or trip
		 * @param deviceName
		 * @returns {string}
		 */
		setDeviceStatusText(deviceName) {
			if (this.device[deviceName] === undefined) {
				return 'OFF';
			}

			if (this.device[deviceName].isTripped) {
				return 'TRIPPED';
			}

			if (this.device[deviceName].isOn) {
				return 'ON';
			}

			if (!this.device[deviceName].isOn) {
				return 'OFF';
			}

			return 'OFF';
		},

		/**
		 * Set device status class.
		 *
		 * @param deviceName
		 * @param activeClass
		 * @param inactiveClass
		 * @param tripClass
		 * @returns {string}
		 */
		setDeviceStatusClass(deviceName, activeClass = 'on', inactiveClass = 'off', tripClass = 'trip') {
			if (this.device[deviceName] === undefined) {
				return inactiveClass
			}

			if (this.device[deviceName].isTripped) {
				return tripClass;
			}

			if (this.device[deviceName].isOn) {
				return activeClass;
			}

			if (!this.device[deviceName].isOn) {
				return inactiveClass;
			}

			return inactiveClass
		},

		/**
		 * Set device status combine class that depends on 2 device.
		 * Example:
		 * deviceName1: LAL105
		 * deviceName2: LAH105
		 *
		 * @param deviceName1
		 * @param deviceName2
		 * @param activeClass
		 * @param inactiveClass
		 * @param tripClass
		 * @returns {*}
		 */
		setDeviceStatusCombineClass(deviceName1, deviceName2, activeClass, inactiveClass, tripClass) {
			if (this.device[deviceName1] === undefined || this.device[deviceName2] === undefined) {
				return inactiveClass;
			}

			if (this.device[deviceName1].status || this.device[deviceName2].status) {
				return activeClass
			}

			if (!this.device[deviceName1].status && !this.device[deviceName2].status) {
				return tripClass
			}


			// if (this.device[deviceName1] === undefined || this.device[deviceName2] === undefined) {
			// 	return inactiveClass;
			// }

			// if (this.device[deviceName1] === undefined || this.device[deviceName2] === undefined) {
			// 	return inactiveClass;
			// }

			// if (this.device[deviceName1].isTripped && this.device[deviceName2].isTripped) {
			// 	return tripClass;
			// }

			// if (this.device[deviceName1].isOn && this.device[deviceName2].isOn) {
			// 	return activeClass;
			// }

			// if (!this.device[deviceName1].isOn && !this.device[deviceName2].isOn) {
			// 	return inactiveClass;
			// }

			return inactiveClass;
		},

		/**
		 * To set device inverter PV. The value is in Hz.
		 */
		setDeviceInverterPV(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-Hz'
			} else {
				if (this.device[deviceName].opHz === undefined || !this.device[deviceName].opHz) {
					return '-Hz'
				}

				return this.device[deviceName].opHz + 'Hz';
			}
		},

		/**
		 * To set device inverter SV. The value is in Hz.
		 */
		setDeviceInverterSV(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-Hz'
			} else {
				if (this.device[deviceName].refHz === undefined || !this.device[deviceName].refHz) {
					return '-Hz'
				}

				return this.device[deviceName].refHz + 'Hz';
			}
		},

		/**
		 * To set device pH. The value is in pH.
		 */
		setDevicePH(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '- pH'
			} else {
				return this.device[deviceName].value + ' pH';
			}
		},

		/**
		 * To set device weight. The value is in kg.
		 */
		setDeviceWeight(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '- kg'
			} else {
				return this.device[deviceName].net + ' kg';
			}
		},

		/**
		 * To set device weight. The value is in kg.
		 */
		setDeviceGrossWeight(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '- kg'
			} else {
				return this.device[deviceName].gross + ' kg';
			}
		},

		/**
		 * To set device level. The value is in percentage (%).
		 */
		setDeviceLevel(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-%'
			} else {
				// HK: should compare against null, use ! operator also matches with value 0
				if (this.device[deviceName].value == null) {
					return '-%'
				} else {
					return this.device[deviceName].value.toFixed(0) + '%';
				}
			}
		},

		/**
		* To set device level that depends on 2 device. Example:
		* deviceName1: LAL105
		* deviceName2: LAH105
		* level = high if LAH105 is true
		* low if LAH105 is false and LAL105 is true,
		* very low if both false
		*/
		setDeviceLevelCombineClass(deviceName1, deviceName2) {
			if (this.device[deviceName1] === undefined || this.device[deviceName2] === undefined) {
				return '-'
			}

			if (this.device[deviceName2].status) {
				return 'High'
			}

			if (!this.device[deviceName2].status && this.device[deviceName1].status) {
				return 'Low'
			}

			if (!this.device[deviceName1].status && !this.device[deviceName2].status) {
				return 'Very Low'
			}

			return '-';
		},

		/**
		* To set device level low or high. Example:
		* status 0: normal
		* status 1: low
		*/
		setDeviceLevelHighLow(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-'
			}

			if (this.device[deviceName].status) {
				return 'Low'
			}
			return 'Normal';
		},

		/**
		 * Set device temp SV.
		 *
		 * @param deviceName
		 * @returns {string}
		 */
		setDeviceTempSV(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-°C'
			} else {
				return this.device[deviceName].sv + '°C';
			}
		},

		/**
		 * Set device temp PV.
		 *
		 * @param deviceName
		 * @returns {string}
		 */
		setDeviceTemp(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-°C'
			} else {
				if (this.device[deviceName].value) {
					return this.device[deviceName].value.toFixed(2) + '°C';
				}
			}
		},

		/**
		 * Set device pressure PV.
		 *
		 * @param deviceName
		 * @param hideUnit
		 * @returns {string|*}
		 */
		setDevicePressure(deviceName, hideUnit = false) {
			if (this.device[deviceName] === undefined) {
				if (hideUnit) {
					return '-'
				}

				return '- barg'
			} else {
				if (hideUnit) {
					return this.device[deviceName].value;
				}

				return this.device[deviceName].value + ' barg';
			}
		},

		/**
		 * Set device pressure SV.
		 *
		 * @param deviceName
		 * @param hideUnit
		 * @returns {string|*}
		 */
		setDevicePressureSV(deviceName, hideUnit = false) {
			if (this.device[deviceName] === undefined) {
				if (hideUnit) {
					return '-'
				}

				return '- barg'
			} else {
				if (hideUnit) {
					return this.device[deviceName].sv;
				}

				return this.device[deviceName].sv + ' barg';
			}
		},

		/**
		 * Set device pressure MV.
		 *
		 * @param deviceName
		 * @param hideUnit
		 * @returns {string|*}
		 */
		setDevicePressureMV(deviceName, hideUnit = false) {
			if (this.device[deviceName] === undefined) {
				if (hideUnit) {
					return '-'
				}

				return '- barg'
			} else {
				if (hideUnit) {
					return this.device[deviceName].mv;
				}

				return this.device[deviceName].mv + ' barg';
			}
		},

		/**
		 * Set device value/
		 *
		 * @param deviceName
		 * @param attribute
		 * @param initialValue
		 * @returns {string|*}
		 */
		setDeviceValue(deviceName, attribute, initialValue = '-') {
			if (this.device[deviceName] === undefined) {
				return initialValue
			}

			return this.device[deviceName][attribute];
		},

		/**
		 * To set device runtime. The value is in hour and minutes.
		 */
		setDeviceRuntime(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-h -m'
			}

			return this.device[deviceName].value;
		},

		/**
		 * To set device level for tank. The value is in percentage (%).
		 */
		setDeviceLevelTank(deviceName) {
			if (this.device[deviceName] === undefined) {
				return 0
			}

			return this.device[deviceName].value;
		},

		/**
		 * To set device volume. The value is in m3.
		 */
		setDeviceVolume(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '- m3'
			}

			return this.round(this.device[deviceName].volume, 2) + ' m3';
		},

		/**
		 * To set camera detected level. The value is in %
		 */
		setCamareLevel(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '- %'
			}

			return this.round(this.device[deviceName].value, 1) + ' %';
		},

		/**
		 * Check if batch controller status idle.
		 * Use for the mqtt topic below
		 * - lgm/data/WT102/bcStatus for DPNR
		 * - lgm/data/WT101/bcStatus for ENR
		 *
		 * @param deviceName
		 */
		checkBatchControllerActive(deviceName) {
			if (this.device[deviceName] === undefined) {
				return false;
			}

			if (this.device[deviceName].controller_status !== 'IDLE') {
				return true;
			};

			return false;
		},

		/**
		 * Check if batch controller status idle.
		 * Use for the mqtt topic below
		 * - lgm/data/WT102/bcStatus for DPNR
		 * - lgm/data/WT101/bcStatus for ENR
		 *
		 * @param deviceName
		 */
		getBatchControllerStatus(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-';
			}
			if (this.device[deviceName].controller_status === "WAIT_ACK") {
				return "PENDING ACTION"
			}

			return this.device[deviceName].controller_status;
		},

		getChillerSupplyTemp(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-';
			}
			if (this.device[deviceName].supplyTemp) {
				return this.device[deviceName].supplyTemp.toFixed(2) + '°C';
			}

			return '-';
		},

		getChillerSetTemp(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-';
			}
			if (this.device[deviceName].setTemp) {
				return this.device[deviceName].setTemp.toFixed(2) + '°C';
			}

			return '-';
		},

		/**
		 * Round number.
		 *
		 * @param value
		 * @param decimals
		 * @returns {number}
		 */
		round(value, decimals) {
			return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
		},

		getCounter(deviceName) {
			if (this.device[deviceName] === undefined) {
				return '-';
			}
			return this.device[deviceName].count;
		}
	}
}