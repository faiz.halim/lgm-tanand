// Vue mixins
export default {
    methods: {
        /**
         * To set device perset mode. Only for DPNR production
         */
        setPresetMode(deviceName) {
            if (this.preset[deviceName] === undefined) {
                return '-'
            } else {
                if (this.preset[deviceName].mode === undefined || !this.preset[deviceName].mode) {
                    return '-'
                }

                return this.preset[deviceName].mode.toUpperCase()
            }
        },

        setPresetValue(deviceName) {
            if (this.preset[deviceName] === undefined) {
                return '-'
            } else {
                if (this.preset[deviceName].value === undefined) {
                    return '-'
                }

                return this.preset[deviceName].value
            }
        },

        setPresetOffset(deviceName) {
            if (this.preset[deviceName] === undefined) {
                return 5
            } else {
                if (this.preset[deviceName].offset === undefined) {
                    return 5
                }
                return this.preset[deviceName].offset
            }
        },
    }
}