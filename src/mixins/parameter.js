// Vue mixins
export default {
    methods: {
        /**
         * Set PV value for parameters in preparation & production page.
         * Need to use function because this data coming from MQTT.
         *
         * @param parameter
         * @returns {string|null|*}
         */
        setPVValueForParameter(parameter) {
            if (parameter.type === 'inverter') {
                let objectKey = parameter.code;

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].opHz != null) {
                    return this.device[objectKey].opHz.toFixed(2) + parameter.unit;
                }

                return '-' + parameter.unit;
            }

            if (parameter.type === 'pid') {
                let objectKey = parameter.code + '_level';

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].value != null) {
                    return this.device[objectKey].value.toFixed(1) + parameter.unit;
                }

                return '-' + parameter.unit;
            }

            if (parameter.type === 'valve') {
                let objectKey = parameter.code;

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].value != null) {
                    return this.device[objectKey].value.toFixed(1) + parameter.unit;
                }

                return '-' + parameter.unit;
            }

            if (parameter.type === 'level') {
                let objectKey = parameter.code;

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].value != null) {
                    return this.device[objectKey].value.toFixed(1) + parameter.unit;
                }

                return '-' + parameter.unit;
            }

            if (parameter.type === 'temp') {
                let objectKey = parameter.code + '_temp';

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].value != null) {
                    return this.device[objectKey].value.toFixed(1) + parameter.unit;
                }

                return '-' + parameter.unit;
            }

            return '';
        },

        /**
         * Set SV value for parameters in preparation & production page.
         * Need to use function because this data coming from MQTT.
         *
         * @param parameter
         * @returns {string|null|*}
         */
        setSVValueForParameter(parameter) {
            if (parameter.type === 'inverter') {
                let objectKey = parameter.code

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].refHz) {
                    return this.device[objectKey].refHz.toFixed(2) + parameter.unit;
                }
            }

            if (parameter.type === 'pid') {
                let objectKey = parameter.code + '_level';

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].sv) {
                    return this.device[objectKey].sv.toFixed(1) + parameter.unit;
                }
            }

            if (parameter.type === 'temp') {
                let objectKey = parameter.code + '_temp';

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].sv != null) {
                    return this.device[objectKey].sv.toFixed(1) + parameter.unit;
                }
            }

            if (parameter.type === 'valve') {
                let objectKey = parameter.code;

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].sv != null) {
                    return this.device[objectKey].sv.toFixed(1) + parameter.unit;
                }
            }

            if (parameter.type === 'level') {
                let objectKey = parameter.code;

                if (this.device[objectKey] === undefined) {
                    return '-' + parameter.unit;
                }

                if (this.device[objectKey] && this.device[objectKey].sv != null) {
                    return this.device[objectKey].sv.toFixed(1) + parameter.unit;
                }
            }

            return '-' + parameter.unit;
        },

        /**
         * Set Status
         *
         * @param parameter
         * @returns {string|null|*}
         */
        setStatusForParameter(parameter) {            
            if (this.device[parameter.code] === undefined) {
                return ['off']
            }

            if (this.device[parameter.code].isOn) {
                return ['on'];
            }

            if (this.device[parameter.code].isTripped) {
                return ['trip'];
            }

            if (!this.device[parameter.code].isOn) {
                return ['off'];
            }

            return ['off'];
        },

        /**
         * Set Temp Mode (Cool Or Hot) for temperature device.
         *
         * @param parameter
         * @returns {string|null|*}
         */
        setTemperatureMode(parameter) {
            if (this.device[parameter] === undefined) {
                return '-'
            }

            if (this.device[parameter] !== undefined && this.device[parameter].operatingMode !== undefined) {
                return this.device[parameter].operatingMode
            }

            return '-'
        },
    }
}