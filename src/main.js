import Vue from "vue";
import Vuex from 'vuex'
import App from "./App.vue";
import router from "./router";

// Vee Validate
import {ValidationObserver, ValidationProvider, extend, localize} from 'vee-validate';
import {required, email, numeric} from 'vee-validate/dist/rules';
import en from 'vee-validate/dist/locale/en.json';

// Vue Sweet Alert 2
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

// Font Awesome
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faChevronLeft,
    faWindowClose,
    faBars,
    faAngleDown,
    faAngleRight,
    faIndustry,
    faBell,
    faFlask,
    faWater,
    faFileContract,
    faCircle,
    faCog,
    faSignOutAlt,
    faAngleDoubleLeft,
    faAngleLeft,
    faAngleDoubleRight,
    faPlus,
    faPencilAlt,
    faSave,
    faMinus,
    faExclamationTriangle,
    faCheckSquare,
    faInfoCircle,
    faTrash,
    faUser
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

// Vue Table 2
import Vuetable from 'vuetable-2'
import VuetablePagination from "./components/table/VuetablePagination";
import VuetablePaginationInfo from 'vuetable-2/src/components/VuetablePaginationInfo';

// Vue Switch
import PrettyCheck from 'pretty-checkbox-vue/check';

// Vue Tooltip
import VTooltip from 'v-tooltip'
// Vue select dropdown
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';

// Import Axios
import axios from 'axios'
import VueAxios from 'vue-axios'

// Vue MQTT
import VueMqtt from 'vue-mqtt';

// Loading overlay
require('js-loading-overlay');

let VueCookie = require('vue-cookie');

// Add require font awesome icon globally.
library.add(faChevronLeft, faWindowClose, faBars, faAngleDown, faAngleRight, faIndustry, faBell, faFlask, faWater, faFileContract, faCircle,
    faCog, faSignOutAlt, faAngleDoubleRight, faAngleDoubleLeft, faAngleLeft, faPlus, faPencilAlt, faSave, faMinus, faExclamationTriangle,
    faCheckSquare, faInfoCircle, faTrash, faUser);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('p-check', PrettyCheck);
Vue.component('v-select', vSelect)

// Vee validate rule and local.
localize('en', en);
extend('email', email);
extend('required', required);
extend('numeric', numeric);

// Load metis menu.
require('metismenu');

Vue.prototype.$JsLoadingOverlay = window.JsLoadingOverlay;
Vue.config.productionTip = false;

Vue.use(VueCookie);
Vue.use(VueSweetalert2);
Vue.use(PrettyCheck);
Vue.use(VueAxios, axios);
Vue.use(VTooltip)

const transformWsUrl = (url, options, client) => {
    client.options.password = localStorage.getItem('token');
    return url;
}

Vue.use(VueMqtt, process.env.VUE_APP_WEBSOCKET_URL, {
    port: process.env.VUE_APP_WEBSOCKET_PORT,
    username: process.env.VUE_APP_WEBSOCKET_USER,
    password: localStorage.getItem('token'),
    keepalive: 0,
    reconnectPeriod: 10000, // 10 seconds interval for next attempt to reconnect if failed.
    transformWsUrl: transformWsUrl
})

Vue.use(Vuex)

import storeData from "./store/index"

const store = new Vuex.Store(
    storeData
)

// Vee validate components.
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

// Vue table 2 components.
Vue.component('Vuetable', Vuetable);
Vue.component('VuetablePagination', VuetablePagination);
Vue.component('VuetablePaginationInfo', VuetablePaginationInfo);

// Vue filter to uppercase first letter for words.
Vue.filter('ucfirst', function (value) {
    return value.substr(0, 1).toUpperCase() + value.substr(1).toLowerCase();
})

Vue.filter('replaceUnderscore', function (value) {
    return value.replace('_', ' ')
})

new Vue({
    store,
    router,
    render: h => h(App),
    methods: {
        refreshToken() {
            let refreshTokenURL = process.env.VUE_APP_API_URL + 'api/auth/refresh';

            this.axios.post(refreshTokenURL, {
                'refreshToken': window.localStorage.getItem('refresh_token')
            }).then(function (response) {
                if (response.status === 200) {
                    window.localStorage.setItem('token', response.data.data.tokens.idToken);
                }
            }).catch(function (error) {
                console.error("Failed to refresh token.", error);
            });
        }
    },
    mounted: function () {
        this.$mqtt.on('error', (error) => {
            console.error('mqtt error', new Date().toLocaleString(), error);
            if (error.toString() === 'Error: Connection refused: Not authorized') {
                this.refreshToken();
            }
        })
    }
}).$mount("#app");
