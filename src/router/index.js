import Vue from "vue";
import VueRouter from "vue-router";
import {resetSidebarMenu, highlightSidebarMenu} from '@/utils/sidebar';
import auth from '../middleware/auth';
import redirectAuth from '../middleware/redirectAuth';

Vue.use(VueRouter);

const routes = [
    /**
     * All routes related to authentication.
     */
    {
        path: "/",
        name: "Auth",
        meta: {
            middleware: [
                redirectAuth
            ]
        },
        component: () =>
            import(/* webpackChunkName: "auth" */"@/views/authentication/Auth.vue")
    },
    /**
     * All routes related to productions.
     */
    {
        path: '/dashboard',
        name: 'dashboard',
        component: () =>
            import(/* webpackChunkName: "dashboard" */"@/views/layouts/Master.vue"),
        meta: {
            middleware: [
                auth
            ]
        },
        children: [
            {
                path: 'preparations/dpnr',
                name: 'preparation-dpnr',
                component: () =>
                    import(/* webpackChunkName: "preparations.dpnr" */"@/views/preparations/dpnr/Dpnr.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'preparations/enr-25',
                name: 'preparation-enr-25',
                component: () =>
                    import(/* webpackChunkName: "preparations.enr25" */"@/views/preparations/enr25/Enr25.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'preparations/enr-50',
                name: 'preparation-enr-50',
                component: () =>
                    import(/* webpackChunkName: "preparations.enr50" */"@/views/preparations/enr50/Enr50.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'productions/dpnr',
                name: 'production-dpnr',
                component: () =>
                    import(/* webpackChunkName: "productions.dpnr" */"@/views/productions/dpnr/Dpnr.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'productions/enr-25',
                name: 'production-enr-25',
                component: () =>
                    import(/* webpackChunkName: "productions.enr25" */"@/views/productions/enr25/Enr25.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'productions/enr-50',
                name: 'production-enr-50',
                component: () =>
                    import(/* webpackChunkName: "productions.enr50" */"@/views/productions/enr50/Enr50.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'tanks',
                name: 'tanks',
                component: () =>
                    import(/* webpackChunkName: "tanks" */"@/views/tanks/Tanks.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'general',
                name: 'general',
                component: () =>
                    import(/* webpackChunkName: "tanks" */"@/views/general/General.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'general-control',
                name: 'general-control',
                component: () =>
                    import(/* webpackChunkName: "tanks" */"@/views/general/GeneralControl.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'audit-logs',
                name: 'audit-logs',
                component: () =>
                    import(/* webpackChunkName: "tanks" */"@/views/audit-logs/AuditLog.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },             
            {
                path: 'settings-recipe',
                name: 'settings-recipe',
                component: () =>
                    import(/* webpackChunkName: "tanks" */"@/views/settings/Recipe.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'settings-automation',
                name: 'settings-automation',
                component: () =>
                    import(/* webpackChunkName: "tanks" */"@/views/settings/Automation.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
            {
                path: 'alert-logs',
                name: 'alert-logs',
                component: () =>
                    import(/* webpackChunkName: "alert-logs" */"@/views/alert-logs/AlertLog.vue"),
                meta: {
                    middleware: [
                        auth
                    ]
                },
            },
        ]
    },
    {
        path: "*",
        name: "404",
        component: () =>
            import(/* webpackChunkName: "page-not-found" */"@/views/error/PageNotFound.vue")
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

// Creates a `nextMiddleware()` function which not only
// runs the default `next()` callback but also triggers
// the subsequent Middleware function.
function nextFactory(context, middleware, index) {
    const subsequentMiddleware = middleware[index];
    // If no subsequent Middleware exists,
    // the default `next()` callback is returned.
    if (!subsequentMiddleware) return context.next;

    return (...parameters) => {
        // Run the default Vue Router `next()` callback first.
        context.next(...parameters);
        // Than run the subsequent Middleware with a new
        // `nextMiddleware()` callback.
        const nextMiddleware = nextFactory(context, middleware, index + 1);
        subsequentMiddleware({...context, next: nextMiddleware});
    };
}

router.beforeEach((to, from, next) => {
    if (to.meta.middleware) {
        const middleware = Array.isArray(to.meta.middleware)
            ? to.meta.middleware
            : [to.meta.middleware];

        const context = {
            from,
            next,
            router,
            to,
        };
        const nextMiddleware = nextFactory(context, middleware, 1);

        return middleware[0]({...context, next: nextMiddleware});
    }

    return next();
});

router.afterEach(() => {
    resetSidebarMenu();
    // After detect route change, highlight the sidebar menu based on the page URL.
    highlightSidebarMenu();
});

export default router;
