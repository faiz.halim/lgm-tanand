export default function redirectAuth({next, router}) {
    if (localStorage.getItem('token')) {
        return router.push({name: 'preparation-dpnr'});
    }

    return next();
}
