const path = require("path");

module.exports = {
	// Set to false if you want to disable eslint. Faster compile time when you disabled.
	lintOnSave: false,
	css: {
		sourceMap: true,
		loaderOptions: {
			sass: {
				prependData: `@import "@/assets/styles/_variables.scss"; @import "@/assets/styles/mixin/_media-query.scss";`
			}
		}
	},
	configureWebpack: {
		devtool: 'source-map'
	}
};
